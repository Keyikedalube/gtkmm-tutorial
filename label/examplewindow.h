#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// child widgets
		Gtk::Box m_hbox;
		Gtk::Box m_vbox, m_vbox2;
		Gtk::Frame m_frame_normal, m_frame_multi, m_frame_left, m_frame_right,
			m_frame_linewrapped, m_frame_filledwrapped, m_frame_underlined;
		Gtk::Label m_label_normal, m_label_multi, m_label_left, m_label_right,
			m_label_linewrapped, m_label_filledwrapped, m_label_underlined;
};

#endif // GTKMM_EXAMPLEWINDOW_H
