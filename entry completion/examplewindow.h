#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// signal handlers
		void on_button_close();

		void on_completion_action_activated(int index);

		// tree model columns, for the EntryCompletion's filter model
		class ModelColumns: public Gtk::TreeModel::ColumnRecord
	{
		public:
			ModelColumns()
			{ add(m_col_id); add(m_col_name); }

			Gtk::TreeModelColumn<unsigned int> m_col_id;
			Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	};
		
		ModelColumns m_Columns;

		typedef std::map<int, Glib::ustring> type_actions_map;
		type_actions_map m_CompletionActions;

		// child widgets
		Gtk::Box m_hbox;
		Gtk::Box m_vbox;
		Gtk::Entry m_entry;
		Gtk::Label m_label;
		Gtk::Button m_button_close;
};

#endif // GTKMM_EXAMPLEWINDOW_H
