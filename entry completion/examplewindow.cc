#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	m_vbox(Gtk::ORIENTATION_VERTICAL),
	m_label("Press a or b to see a list of possible completions and actions."),
	m_button_close("Close")
{
	//set_size_request(200, 100);
	set_title("Gtk::EntryCompletion");

	add(m_vbox);
	m_vbox.pack_start(m_entry, Gtk::PACK_SHRINK);

	m_vbox.pack_start(m_label, Gtk::PACK_EXPAND_WIDGET);

	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_close));
	m_vbox.pack_start(m_button_close, Gtk::PACK_SHRINK);
	m_button_close.set_can_default();
	m_button_close.grab_default();

	// add an EntryCompletion
	auto completion = Gtk::EntryCompletion::create();
	m_entry.set_completion(completion);

	// create and fill the completion's filler model
	auto refCompletionModel = Gtk::ListStore::create(m_Columns);
	completion->set_model(refCompletionModel);

	// fill the TreeView's model
	Gtk::TreeModel::Row row = *(refCompletionModel->append());
	row[m_Columns.m_col_id] = 1;
	row[m_Columns.m_col_name] = "Alan Zebedee";

	row = *(refCompletionModel->append());
	row[m_Columns.m_col_id] = 2;
	row[m_Columns.m_col_name] = "Adrian Boo";

	row = *(refCompletionModel->append());
	row[m_Columns.m_col_id] = 3;
	row[m_Columns.m_col_name] = "Bob McRoberts";

	row = *(refCompletionModel->append());
	row[m_Columns.m_col_id] = 4;
	row[m_Columns.m_col_name] = "Box McBob";

	// tell the completion what model column to use to
	// - look for a match (when we use the default matching,
	// instead of set_match_func()
	// - display text in the entry when a match is found
	completion->set_text_column(m_Columns.m_col_name);

	// add actions to the completion
	// these are just extra items shown at the bottom of the list
	// of possible completions
	//
	// remember them for later
	m_CompletionActions[0] = "Use Wizard";
	m_CompletionActions[1] = "Browse for Filename";

	for (const auto& the_pair: m_CompletionActions) {
		auto position = the_pair.first;
		auto title = the_pair.second;
		completion->insert_action_text(title, position);
	}

	completion->signal_action_activated().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_completion_action_activated));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_close()
{
	hide();
}

void ExampleWindow::on_completion_action_activated(int index)
{
	type_actions_map::iterator iter = m_CompletionActions.find(index);
	if (iter != m_CompletionActions.end()) {
		// if it's in the map
		Glib::ustring title = iter->second;
		std::cout << "Action selected: " << title << std::endl;
	}
}
