#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		void fill_buffers();

		/* signal handlers */
		void on_button_quit();
		void on_button_buffer1();
		void on_button_buffer2();

		/* child widgets */
		Gtk::Box v_box;

		Gtk::ScrolledWindow scrolledwindow;
		Gtk::TextView textview;

		Glib::RefPtr<Gtk::TextBuffer> ref_textbuffer1, ref_textbuffer2;

		Gtk::ButtonBox buttonbox;
		Gtk::Button button_quit, button_buffer1, button_buffer2;
};

#endif // GTKMM_EXAMPLEWINDOW_H
