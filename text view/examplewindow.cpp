#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	button_quit("_Quit", true),
	button_buffer1("Use buffer 1"),
	button_buffer2("Use buffer 2")
{
	set_title("Gtk::TextView example");
	set_border_width(5);
	set_default_size(400, 200);

	add(v_box);

	scrolledwindow.add(textview);

	scrolledwindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	v_box.pack_start(scrolledwindow);
	v_box.pack_start(buttonbox, Gtk::PACK_SHRINK);
	
	buttonbox.pack_start(button_buffer1, Gtk::PACK_SHRINK);
	buttonbox.pack_start(button_buffer2, Gtk::PACK_SHRINK);
	buttonbox.pack_start(button_quit, Gtk::PACK_SHRINK);
	buttonbox.set_border_width(5);
	buttonbox.set_spacing(5);
	buttonbox.set_layout(Gtk::BUTTONBOX_END);

	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));
	button_buffer1.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_buffer1));
	button_buffer2.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_buffer2));

	fill_buffers();
	on_button_buffer1();

	show_all_children();
}

void ExampleWindow::fill_buffers()
{
	ref_textbuffer1 = Gtk::TextBuffer::create();
	ref_textbuffer1->set_text("This is the text from TextBuffer 1");

	ref_textbuffer2 = Gtk::TextBuffer::create();
	ref_textbuffer2->set_text("This is the text from TextBuffer 2");
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}

void ExampleWindow::on_button_buffer1()
{
	textview.set_buffer(ref_textbuffer1);
}

void ExampleWindow::on_button_buffer2()
{
	textview.set_buffer(ref_textbuffer2);
}
