#ifndef GTKMM_EXAMPLE_RADIOBUTTONS_H
#define GTKMM_EXAMPLE_RADIOBUTTONS_H

#include <gtkmm/box.h>
#include <gtkmm/window.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/separator.h>

class RadioButtons: public Gtk::Window
{
	public:
		RadioButtons();
		virtual ~RadioButtons();

	protected:
		// signal handlers
		void on_button_clicked();

		// child widgets
		Gtk::Box m_box_top, m_box1, m_box2;
		Gtk::RadioButton m_rb1, m_rb2, m_rb3;
		Gtk::Separator m_sep;
		Gtk::Button m_button_close;
};

#endif // GTKMM_EXAMPLE_RADIOBUTTONS_H
