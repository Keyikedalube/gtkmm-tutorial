#include "radiobuttons.h"

RadioButtons::RadioButtons():
	m_box_top(Gtk::ORIENTATION_VERTICAL),
	m_box1(Gtk::ORIENTATION_VERTICAL, 10),
	m_box2(Gtk::ORIENTATION_VERTICAL, 10),
	m_rb1("button1"),
	m_rb2("button2"),
	m_rb3("button3"),
	m_button_close("close")
{
	// set title and border of the window
	set_title("radio buttons");
	set_border_width(0);

	// put radio buttons 2 and 3 in the same group as 1
	m_rb2.join_group(m_rb1);
	m_rb3.join_group(m_rb1);

	// add outer box to the window
	// because the window can only contain a single widget
	add(m_box_top);

	// put the inner boxes and the separator in the outer box
	m_box_top.pack_start(m_box1);
	m_box_top.pack_start(m_sep);
	m_box_top.pack_start(m_box2);

	// set the inner boxes' borders
	m_box2.set_border_width(10);
	m_box1.set_border_width(10);

	// put the radio buttons in box1
	m_box1.pack_start(m_rb1);
	m_box1.pack_start(m_rb2);
	m_box1.pack_start(m_rb3);

	// set the second button active
	m_rb2.set_active();

	// put close button in box2
	m_box2.pack_start(m_button_close);

	// make the button the default widget
	m_button_close.set_can_default();
	m_button_close.grab_default();

	// connect the clicked signal of the button to
	// RadioButtons::on_button_clicked()
	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&RadioButtons::on_button_clicked));

	// show all children of the window
	show_all_children();
}

RadioButtons::~RadioButtons()
{
}

void RadioButtons::on_button_clicked()
{
	hide(); // to close the application
}
