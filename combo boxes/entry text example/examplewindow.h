#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/comboboxtext.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_combo_changed();
		void on_entry_activate();
		bool on_entry_focus_out_event(GdkEventFocus *);

		/* signal connection */
		sigc::connection connection_focus_out;

		/* child widgets */
		Gtk::ComboBoxText combo;
};

#endif // GTKMM_EXAMPLEWINDOW_H
