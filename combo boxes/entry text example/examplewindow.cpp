#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	combo(true /* has entry */)
{
	set_title("ComboBoxText example");

	combo.append("something");
	combo.append("something else");
	combo.append("something or other");
	combo.set_active(0);

	add(combo);

	auto entry = combo.get_entry();
	combo.signal_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_combo_changed));
	if (entry) {
		entry->add_events(Gdk::FOCUS_CHANGE_MASK);
		entry->signal_activate().connect(sigc::mem_fun(*this,
					&ExampleWindow::on_entry_activate));
		connection_focus_out = entry->signal_focus_out_event().connect(
				sigc::mem_fun(*this, &ExampleWindow::on_entry_focus_out_event));
	} else
		std::cout << "No entry???" << std::endl;

	combo.property_has_frame() = false;
	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
	connection_focus_out.disconnect();
}

void ExampleWindow::on_combo_changed()
{
	std::cout << "on_combo_changed(): Row = " << combo.get_active_row_number()
		<< ", Text = " << combo.get_active_text() << std::endl;
}

void ExampleWindow::on_entry_activate()
{
	std::cout << "on_entry_activate(): Row = " << combo.get_active_row_number()
		<< ", Text = " << combo.get_active_text() << std::endl;
}

bool ExampleWindow::on_entry_focus_out_event(GdkEventFocus *event)
{
	std::cout << "on_entry_focus_out_event(): Row = " << combo.get_active_row_number()
		<< ", Text = " << combo.get_active_text() << std::endl;
	return true;
}
