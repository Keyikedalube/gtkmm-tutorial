#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow()
{
	set_title("ComboBox example");

	ref_treemodel = Gtk::ListStore::create(columns);
	combo.set_model(ref_treemodel);

	Gtk::TreeModel::Row row = *(ref_treemodel->append());
	row[columns.col_id] = 1;
	row[columns.col_name] = "Billy Bob";
	row[columns.col_extra] = "Something";
	combo.set_active(row);

	row = *(ref_treemodel->append());
	row[columns.col_id] = 2;
	row[columns.col_name] = "Joey Jojo";
	row[columns.col_extra] = "Yada";

	row = *(ref_treemodel->append());
	row[columns.col_id] = 3;
	row[columns.col_name] = "Rob McRoberts";
	row[columns.col_extra] = "";

	combo.pack_start(columns.col_id);
	combo.pack_start(columns.col_name);

	/* an example of adding a cell renderer manually
	 * instead of using pack_start(model_column)
	 * so we have more control
	 */
	combo.set_cell_data_func(cell, sigc::mem_fun(*this,
				&ExampleWindow::on_cell_data_extra));
	combo.pack_start(cell);

	add(combo);

	combo.signal_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_combo_changed));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_cell_data_extra(const Gtk::TreeModel::const_iterator &iter)
{
	auto row= *iter;
	const Glib::ustring extra = row[columns.col_extra];

	/* transform the value, deciding how to represent it as text */
	if (extra.empty())
		cell.property_text() = "(none)";
	else
		cell.property_text() = "-" + extra + "-";

	/* change other cell renderer properties too */
	cell.property_foreground() = (extra == "Yada" ? "Red" : "Green");
}

void ExampleWindow::on_combo_changed()
{
	Gtk::TreeModel::iterator iter = combo.get_active();

	if (iter) {
		Gtk::TreeModel::Row row = *iter;

		if (row) {
			int id = row[columns.col_id];
			Glib::ustring name = row[columns.col_name];

			std::cout << " ID = " << id << ", name = " << name << std::endl;
		}
	} else
		std::cout << "invalid iter" << std::endl;
}
