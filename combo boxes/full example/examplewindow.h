#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/liststore.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		void on_cell_data_extra(const Gtk::TreeModel::const_iterator &);

		/* signal handlers */
		void on_combo_changed();

		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
					add(col_extra);
				}
				Gtk::TreeModelColumn<int> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
				Gtk::TreeModelColumn<Glib::ustring> col_extra;
		};

		ModelColumns columns;

		/* child widgets */
		Gtk::ComboBox combo;
		Gtk::CellRendererText cell;
		Glib::RefPtr<Gtk::ListStore> ref_treemodel;
};

#endif // GTKMM_EXAMPLEWINDOW_H
