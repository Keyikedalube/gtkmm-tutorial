#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow()
{
	set_title("ComboBoxText example");

	combo.append("something");
	combo.append("something else");
	combo.append("something or other");
	combo.set_active(1);

	add(combo);

	combo.signal_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_combo_changed));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_combo_changed()
{
	Glib::ustring text = combo.get_active_text();
	if (!(text.empty()))
		std::cout << "Combo changed: " << text << std::endl;
}
