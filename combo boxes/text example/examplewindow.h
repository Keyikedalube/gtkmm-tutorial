#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/comboboxtext.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_combo_changed();

		/* child widgets */
		Gtk::ComboBoxText combo;
};

#endif // GTKMM_EXAMPLEWINDOW_H
