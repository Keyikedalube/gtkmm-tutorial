#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/combobox.h>
#include <gtkmm/treestore.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_entry_changed();
		void on_entry_activate();
		bool on_entry_focus_out_event(GdkEventFocus *);

		/* signal connection */
		sigc::connection connection_focus_out;

		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
				}

				Gtk::TreeModelColumn<Glib::ustring> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
		};

		ModelColumns columns;

		/* child widgets */
		Gtk::ComboBox combo;
		Glib::RefPtr<Gtk::TreeStore> ref_treemodel;
};

#endif // GTKMM_EXAMPLEWINDOW_H
