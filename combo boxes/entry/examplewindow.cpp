#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	combo(true /* has_entry */)
{
	set_title("ComboBox example");

	ref_treemodel = Gtk::TreeStore::create(columns);
	combo.set_model(ref_treemodel);

	Gtk::TreeModel::Row row = *(ref_treemodel->append());
	row[columns.col_id] = "1";
	row[columns.col_name] = "Billy Bob";

	row = *(ref_treemodel->append());
	row[columns.col_id] = "2";
	row[columns.col_name] = "Joey Jojo";

	row = *(ref_treemodel->append());
	row[columns.col_id] = "3";
	row[columns.col_name] = "Rob McRoberts";

	combo.pack_start(columns.col_id);
	combo.pack_start(columns.col_name);

	combo.set_entry_text_column(columns.col_id);
	combo.set_active(1);

	add(combo);

	auto entry = combo.get_entry();
	if (entry) {
		/* the entry shall receive focus out events */
		entry->add_events(Gdk::FOCUS_CHANGE_MASK);

		/* alternatively you can connect to combo.signal_changed() */
		entry->signal_changed().connect(sigc::mem_fun(*this,
					&ExampleWindow::on_entry_changed));
		entry->signal_activate().connect(sigc::mem_fun(*this,
					&ExampleWindow::on_entry_activate));
		connection_focus_out = entry->signal_focus_out_event().connect(
				sigc::mem_fun(*this, &ExampleWindow::on_entry_focus_out_event));
	} else
		std::cout << "No entry??? " << std::endl;

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
	/* the focus_out signal may be emitted while combo is being destructed
	 * the signal handler can generate critical messages, it it's called when
	 * combo has been partially destructed
	 */
	connection_focus_out.disconnect();
}

void ExampleWindow::on_entry_changed()
{
	auto entry = combo.get_entry();

	if (entry) {
		std::cout << "on_entry_changed(): Row = " << combo.get_active_row_number()
			<< ", ID = " << entry->get_text() << std::endl;
	}
}

void ExampleWindow::on_entry_activate()
{
	auto entry = combo.get_entry();

	if (entry) {
		std::cout << "on_entry_activate(): Row = " << combo.get_active_row_number()
			<< ", ID = " << entry->get_text() << std::endl;
	}
}

bool ExampleWindow::on_entry_focus_out_event(GdkEventFocus *event)
{
	auto entry = combo.get_entry();

	if (entry) {
		std::cout << "on_entry_focus_out_event(): Row = " << combo.get_active_row_number()
			<< ", ID = " << entry->get_text() << std::endl;
		return true;
	}
	return false;
}
