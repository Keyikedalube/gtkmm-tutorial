#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// signal handlers
		void on_checkbutton_snap();
		void on_checkbutton_numeric();
		void on_spinbutton_digits_changed();
		void on_button_close();

		enum enumValueFormats
		{
			VALUE_FORMAT_INT,
			VALUE_FORMAT_FLOAT
		};
		void on_button_getvalue(enumValueFormats display);

		// child widgets
		Gtk::Frame m_frame_notaccelerated, m_frame_accelerated;
		Gtk::Box m_hbox_notaccelerated, m_hbox_accelerated,
			m_hbox_buttons;
		Gtk::Box m_vbox_main, m_vbox, m_vbox_day, m_vbox_month, m_vbox_year,
			m_vbox_accelerated, m_vbox_value, m_vbox_digits;
		Gtk::Label m_label_day, m_label_month, m_label_year,
			m_label_value, m_label_digits, m_label_showvalue;
		Glib::RefPtr<Gtk::Adjustment> m_adjustment_day, m_adjustment_month, m_adjustment_year,
			m_adjustment_value, m_adjustment_digits;
		Gtk::SpinButton m_spinbutton_day, m_spinbutton_month, m_spinbutton_year,
			m_spinbutton_value, m_spinbutton_digits;
		Gtk::CheckButton m_checkbutton_snap, m_checkbutton_numeric;
		Gtk::Button m_button_int, m_button_float, m_button_close;
};

#endif // GTKMM_EXAMPLEWINDOW_H
