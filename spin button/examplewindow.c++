#include "examplewindow.h"
#include <iostream>
#include <cstdio>

ExampleWindow::ExampleWindow():
	m_frame_notaccelerated("Not accelerated"),
	m_frame_accelerated("Accelerated"),
	m_vbox_main(Gtk::ORIENTATION_VERTICAL, 5),
	m_vbox(Gtk::ORIENTATION_VERTICAL),
	m_vbox_day(Gtk::ORIENTATION_VERTICAL),
	m_vbox_month(Gtk::ORIENTATION_VERTICAL),
	m_vbox_year(Gtk::ORIENTATION_VERTICAL),
	m_vbox_accelerated(Gtk::ORIENTATION_VERTICAL),
	m_vbox_value(Gtk::ORIENTATION_VERTICAL),
	m_vbox_digits(Gtk::ORIENTATION_VERTICAL),
	m_label_day("Day: ", Gtk::ALIGN_START),
	m_label_month("Month: ", Gtk::ALIGN_START),
	m_label_year("Year: ", Gtk::ALIGN_START),
	m_label_value("Value: ", Gtk::ALIGN_START),
	m_label_digits("Digits: ", Gtk::ALIGN_START),
	m_adjustment_day(Gtk::Adjustment::create(1.0, 1.0, 31.0, 1.0, 5.0, 0.0)),
	m_adjustment_month(Gtk::Adjustment::create(1.0, 1.0, 12.0, 1.0, 5.0, 0.0)),
	m_adjustment_year(Gtk::Adjustment::create(2012.0, 2200.0, 1.0, 100.0, 0.0)),
	m_adjustment_value(Gtk::Adjustment::create(0.0, -10000.0, 10000.0, 0.5, 100.0, 0.0)),
	m_adjustment_digits(Gtk::Adjustment::create(2.0, 1.0, 5.0, 1.0, 1.0, 0.0)),
	m_spinbutton_day(m_adjustment_day),
	m_spinbutton_month(m_adjustment_month),
	m_spinbutton_year(m_adjustment_year),
	m_spinbutton_value(m_adjustment_value, 1.0, 2),
	m_spinbutton_digits(m_adjustment_digits),
	m_checkbutton_snap("Snap to 0.5 ticks"),
	m_checkbutton_numeric("Numeric only input mode"),
	m_button_int("Value as Int"),
	m_button_float("Value as Float"),
	m_button_close("Close")
{
	set_title("SpinButton");

	m_vbox_main.set_border_width(10);
	add(m_vbox_main);

	m_vbox.set_border_width(5);
	m_frame_notaccelerated.add(m_vbox);

	// Day, month, year spinners
	m_vbox.pack_start(m_hbox_notaccelerated, Gtk::PACK_EXPAND_WIDGET, 5);

	m_vbox_day.pack_start(m_label_day);

	m_spinbutton_day.set_wrap();

	m_vbox_day.pack_start(m_spinbutton_day);

	m_hbox_notaccelerated.pack_start(m_vbox_day, Gtk::PACK_EXPAND_WIDGET, 5);

	m_vbox_month.pack_start(m_label_month);

	m_spinbutton_month.set_wrap();
	m_vbox_month.pack_start(m_spinbutton_month);

	m_hbox_notaccelerated.pack_start(m_vbox_month, Gtk::PACK_EXPAND_WIDGET, 5);

	m_vbox_year.pack_start(m_label_year);

	m_spinbutton_year.set_size_request(55, -1);
	m_vbox_year.pack_start(m_spinbutton_year);

	m_hbox_notaccelerated.pack_start(m_vbox_year, Gtk::PACK_EXPAND_WIDGET, 5);

	// accelerated
	m_vbox_main.pack_start(m_frame_accelerated);

	m_vbox_accelerated.set_border_width(5);
	m_frame_accelerated.add(m_vbox_accelerated);

	m_vbox_accelerated.pack_start(m_hbox_accelerated, Gtk::PACK_EXPAND_WIDGET, 5);

	m_hbox_accelerated.pack_start(m_vbox_value, Gtk::PACK_EXPAND_WIDGET, 5);

	m_vbox_value.pack_start(m_label_value);

	m_spinbutton_value.set_wrap();
	m_spinbutton_value.set_size_request(100, -1);
	m_vbox_value.pack_start(m_spinbutton_value);

	m_hbox_accelerated.pack_start(m_vbox_digits, Gtk::PACK_EXPAND_WIDGET, 5);

	m_vbox_digits.pack_start(m_label_digits);

	m_spinbutton_digits.set_wrap();
	m_adjustment_digits->signal_value_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_spinbutton_digits_changed));

	m_vbox_digits.pack_start(m_spinbutton_digits);

	// checkbuttons
	m_vbox_accelerated.pack_start(m_checkbutton_snap);
	m_checkbutton_snap.set_active();
	m_checkbutton_snap.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbutton_snap));

	m_vbox_accelerated.pack_start(m_checkbutton_numeric);
	m_checkbutton_numeric.set_active();
	m_checkbutton_numeric.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbutton_numeric));

	// buttons
	m_vbox_accelerated.pack_start(m_hbox_buttons, Gtk::PACK_SHRINK, 5);

	m_button_int.signal_clicked().connect(sigc::bind(sigc::mem_fun(*this,
					&ExampleWindow::on_button_getvalue), VALUE_FORMAT_INT));
	m_hbox_buttons.pack_start(m_button_int, Gtk::PACK_EXPAND_WIDGET, 5);

	m_button_float.signal_clicked().connect(sigc::bind(sigc::mem_fun(*this,
					&ExampleWindow::on_button_getvalue), VALUE_FORMAT_FLOAT));
	m_hbox_buttons.pack_start(m_button_float, Gtk::PACK_EXPAND_WIDGET, 5);

	m_vbox_accelerated.pack_start(m_label_showvalue);
	m_label_showvalue.set_text("0");

	// close button
	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_close));
	m_vbox_main.pack_start(m_button_close, Gtk::PACK_SHRINK);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_close()
{
	hide();
}

void ExampleWindow::on_checkbutton_snap()
{
	m_spinbutton_value.set_snap_to_ticks(m_checkbutton_snap.get_active());
}

void ExampleWindow::on_checkbutton_numeric()
{
	m_spinbutton_value.set_numeric(m_checkbutton_numeric.get_active());
}

void ExampleWindow::on_spinbutton_digits_changed()
{
	m_spinbutton_value.set_digits(m_spinbutton_digits.get_value_as_int());
}

void ExampleWindow::on_button_getvalue(enumValueFormats display)
{
	gchar buf[32];

	if (display == VALUE_FORMAT_INT)
		sprintf(buf, "%d", m_spinbutton_value.get_value_as_int());
	else
		sprintf(buf, "%0.*f", m_spinbutton_value.get_digits(),
				m_spinbutton_value.get_value());

	m_label_showvalue.set_text(buf);
}
