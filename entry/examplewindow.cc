#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	m_vbox(Gtk::ORIENTATION_VERTICAL),
	m_button_close("Close"),
	m_checkbutton_editable("Editable"),
	m_checkbutton_visible("Visible")
{
	set_size_request(200, 100);
	set_title("Gtk::Entry");

	add(m_vbox);

	m_entry.set_max_length(50);
	m_entry.set_text("hello");
	m_entry.set_text(m_entry.get_text() + " world");
	m_entry.select_region(0, m_entry.get_text_length());
	m_vbox.pack_start(m_entry);

	// note that add() can also be use instead of pack_xxx()
	m_vbox.add(m_hbox);

	m_hbox.pack_start(m_checkbutton_editable);
	m_checkbutton_editable.signal_toggled().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbox_editable_toggled));
	m_checkbutton_editable.set_active(true);

	m_hbox.pack_start(m_checkbutton_visible);
	m_checkbutton_visible.signal_toggled().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbox_visibility_toggled));
	m_checkbutton_visible.set_active(true);

	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_close));
	m_vbox.pack_start(m_button_close);
	m_button_close.set_can_default();
	m_button_close.grab_default();

	show_all_children();
}

ExampleWindow::	~ExampleWindow()
{
}

void ExampleWindow::on_checkbox_editable_toggled()
{
	m_entry.set_editable(m_checkbutton_editable.get_active());
}

void ExampleWindow::on_checkbox_visibility_toggled()
{
	m_entry.set_visibility(m_checkbutton_visible.get_active());
}

void ExampleWindow::on_button_close()
{
	hide();
}

