#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow : public Gtk::Window
{
	public:
  		ExampleWindow();
  		virtual ~ExampleWindow();

	protected:
  		//Signal handlers:
  		void on_checkbox_editable_toggled();
  		void on_checkbox_visibility_toggled();
  		void on_button_close();

		//Child widgets:
		Gtk::Box m_hbox;
		Gtk::Box m_vbox;
		Gtk::Entry m_entry;
		Gtk::Button m_button_close;
		Gtk::CheckButton m_checkbutton_editable, m_checkbutton_visible;
};

#endif // GTKMM_EXAMPLEWINDOW_H

