#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Dialog {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_dialog_response(int response_id);

		/* child widgets */
		Gtk::ScrolledWindow m_scrolled_window;
		Gtk::Grid m_grid;
};

#endif // GTKMM_EXAMPLEWINDOW_H
		
