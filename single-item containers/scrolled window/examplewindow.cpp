#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow()
{
	set_title("Gtk::ScrolledWindow example");
	set_border_width(0);
	set_size_request(300, 300);

	m_scrolled_window.set_border_width(10);

	m_scrolled_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);

	get_content_area()->pack_start(m_scrolled_window);

	m_grid.set_row_spacing(10);
	m_grid.set_column_spacing(10);

	m_scrolled_window.add(m_grid);

	/* this simply creates a grid of toggle buttons
	 * to demonstrate the scrolled window
	 */
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			char buffer[32];
			sprintf(buffer, "button (%d, %d)\n", i, j);
			auto p_button = Gtk::make_managed<Gtk::ToggleButton>(buffer);
			m_grid.attach(*p_button, i, j);
		}
	}

	add_button("_Close", Gtk::RESPONSE_CLOSE);
	signal_response().connect(sigc::mem_fun(*this, &ExampleWindow::on_dialog_response));
	set_default_response(Gtk::RESPONSE_CLOSE);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_dialog_response(int response_id)
{
	switch (response_id) {
		case Gtk::RESPONSE_CLOSE:
		case Gtk::RESPONSE_DELETE_EVENT:
			hide();
			break;
		default:
			std::cout << "Unexpected response_id = " << response_id << std::endl;
			break;
	}
}
