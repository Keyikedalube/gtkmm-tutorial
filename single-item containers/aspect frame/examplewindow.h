#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* child widgets */
		Gtk::AspectFrame m_aspect_frame;
		Gtk::DrawingArea m_drawing_area;
};

#endif // GTKMM_EXAMPLEWINDOW_H
