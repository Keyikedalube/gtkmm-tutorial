#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	m_aspect_frame("2x1", /* label */
			Gtk::ALIGN_CENTER, /* x */
			Gtk::ALIGN_CENTER, /* y */
			2.0, /* xsize/ysize = 2 */
			false /* ignore child's aspect */
		      )
{
	set_title("Aspect Frame");
	set_border_width(10);

	/* add a child widget to the aspect frame
	 * ask for 200x200 window, but the AspectFrame will give us a 200x100
	 * window since we are forcing a 2x1 aspect ratio
	 */
	m_drawing_area.set_size_request(200, 200);
	m_aspect_frame.add(m_drawing_area);

	add(m_aspect_frame);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}
