#include "examplewindow.h"

ExampleWindow::ExampleWindow()
{
	set_title("Frame example");
	set_size_request(300, 200);
	set_border_width(10);

	add(m_frame);

	m_frame.set_label("Gtk::Frame widget");

	m_frame.set_shadow_type(Gtk::SHADOW_ETCHED_OUT);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}
