#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	m_button("_Close", /* mnemonic = */ true)
{
	set_title("Alignment");
	set_border_width(10);
	set_default_size(200, 50);

	m_button.set_halign(Gtk::ALIGN_END);
	m_button.set_valign(Gtk::ALIGN_CENTER);
	add(m_button);

	m_button.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_clicked));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_clicked()
{
	hide();
}
