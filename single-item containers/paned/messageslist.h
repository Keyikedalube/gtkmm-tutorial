#ifndef GTKMM_EXAMPLE_MESSAGESLIST_H
#define GTKMM_EXAMPLE_MESSAGESLIST_H

#include <gtkmm.h>

class MessagesList: public Gtk::ScrolledWindow {
	public:
		MessagesList();
		virtual ~MessagesList();

		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
		public:
			ModelColumns()
			{ add(m_col_text); }

			Gtk::TreeModelColumn<Glib::ustring> m_col_text;
		};

		ModelColumns m_columns;

	protected:
		Glib::RefPtr<Gtk::ListStore> m_ref_list_store; // the tree model
		Gtk::TreeView m_tree_view; // the tree view
};

#endif // GTKMM_EXAMPLE_MESSAGESLIST_H
