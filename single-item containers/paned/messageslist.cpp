#include "messageslist.h"
#include <sstream>

MessagesList::MessagesList()
{
	/* create a new scrolled window, with scrollbars only if needed */
	set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	add(m_tree_view);

	/* create a list store */
	m_ref_list_store = Gtk::ListStore::create(m_columns);

	m_tree_view.set_model(m_ref_list_store);

	/* add some messages to the window */
	for (int i = 0; i < 10; ++i) {
		std::ostringstream text;
		text << "message #" << i;

		Gtk::TreeModel::Row row = *(m_ref_list_store->append());
		row[m_columns.m_col_text] = text.str();
	}

	/* add the model's column to the view's columns */
	m_tree_view.append_column("Messages", m_columns.m_col_text);

	show_all_children();
}

MessagesList::~MessagesList()
{
}
