#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include "messageslist.h"
#include "messagetext.h"
#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// child widgets
		Gtk::Paned m_vertical_paned;
		MessagesList m_messages_list;
		MessageText m_message_text;
};

#endif // GTKMM_EXAMPLEWINDOW_H
