#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	m_vertical_paned(Gtk::ORIENTATION_VERTICAL)
{
	set_title("Paned windows");
	set_border_width(10);
	set_default_size(450, 400);

	/* add a vertical paned widget to out top level window */
	add(m_vertical_paned);

	/* now add the contents of the two halves of the window */
	m_vertical_paned.add1(m_messages_list);
	m_vertical_paned.add2(m_message_text);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}
