cmake_minimum_required(VERSION 3.15)

project(header-bar)

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})

# 1. Our custom target implementation for compiling exampleapp resources file
find_program(GLIB_COMPILE_RESOURCES NAMES glib-compile-resources REQUIRED)
mark_as_advanced(GLIB_COMPILE_RESOURCES)

set(GRESOURCE_C exampleapp.gresource.c)
set(GRESOURCE_XML exampleapp.gresource.xml)

add_custom_command(
	OUTPUT ${GRESOURCE_C}
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	COMMAND ${GLIB_COMPILE_RESOURCES}
	ARGS
		--generate-source
		--target=${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
		${GRESOURCE_XML}
	VERBATIM
	MAIN_DEPENDENCY ${GRESOURCE_XML}
	DEPENDS
		app_menu.ui
		gears_menu.ui
		prefs.ui
		window.ui
		exampleapp.png
)

# 2. Custom target implementation for compiling our exampleapp settings file
find_program(GLIB_COMPILE_SCHEMAS NAMES glib-compile-schemas REQUIRED)
mark_as_advanced(GLIB_COMPILE_SCHEMAS)

set(GSCHEMA_COMPILED gschema.compiled)
set(GSCHEMA_XML org.gtkmm.exampleapp.gschema.xml)

add_custom_command(
	OUTPUT ${GSCHEMA_COMPILED}
	COMMAND ${GLIB_COMPILE_SCHEMAS}
	ARGS
		${CMAKE_CURRENT_SOURCE_DIR}
		--targetdir=${CMAKE_CURRENT_BINARY_DIR}
	VERBATIM
	MAIN_DEPENDENCY ${GSCHEMA_XML}
)

# Then set our custom target
add_custom_target(
	exampleapp-resource
	DEPENDS ${GRESOURCE_C}
)

add_custom_target(
	exampleapp-gschema
	DEPENDS ${GSCHEMA_COMPILED}
)

# Finally, build our project target
set(EXAMPLEAPP_SOURCES
	main.cpp
	exampleapplication.cpp
	exampleappwindow.cpp
	exampleappprefs.cpp
	exampleapp.gresource.c
	)
add_executable(${PROJECT_NAME} ${EXAMPLEAPP_SOURCES})
add_dependencies(${PROJECT_NAME} exampleapp-resource exampleapp-gschema)
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES})

