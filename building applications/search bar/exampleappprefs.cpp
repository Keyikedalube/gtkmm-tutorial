#include "exampleappprefs.h"
#include "exampleappwindow.h"
#include <stdexcept>

ExampleAppPrefs::ExampleAppPrefs(BaseObjectType* cobject,
  const Glib::RefPtr<Gtk::Builder>& refBuilder)
: Gtk::Dialog(cobject),
  m_refBuilder(refBuilder),
  m_settings(),
  m_font(nullptr),
  m_transition(nullptr)
{
  m_refBuilder->get_widget("font", m_font);
  if (!m_font)
    throw std::runtime_error("No \"font\" object in prefs.ui");

  m_refBuilder->get_widget("transition", m_transition);
  if (!m_transition)
    throw std::runtime_error("No \"transition\" object in prefs.ui");

  m_settings = Gio::Settings::create("org.gtkmm.exampleapp");
  m_settings->bind("font", m_font->property_font_name());
  m_settings->bind("transition", m_transition->property_active_id());
}

//static
ExampleAppPrefs* ExampleAppPrefs::create(Gtk::Window& parent)
{
  // Load the Builder file and instantiate its widgets.
  auto refBuilder = Gtk::Builder::create_from_resource("/org/gtkmm/exampleapp/prefs.ui");

  ExampleAppPrefs* dialog = nullptr;
  refBuilder->get_widget_derived("prefs_dialog", dialog);
  if (!dialog)
    throw std::runtime_error("No \"prefs_dialog\" object in prefs.ui");

  dialog->set_transient_for(parent);

  return dialog;
}
