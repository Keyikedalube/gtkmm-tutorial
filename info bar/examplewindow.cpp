#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	m_vbox(Gtk::ORIENTATION_VERTICAL, 6),
	m_button_quit("_Quit", true),
	m_button_clear("_Clear", true)
{
	set_title("Gtk::InfoBar Example");
	set_border_width(6);
	set_default_size(400, 200);

	add(m_vbox);

	// add the message label to the InfoBar
	auto info_bar_container = dynamic_cast<Gtk::Container*>(m_infobar.get_content_area());
	if (info_bar_container)
		info_bar_container->add(m_message_label);

	// add an ok button to the InfoBar
	m_infobar.add_button("_OK", 0);

	// add the InfoBar to the vbox
	m_vbox.pack_start(m_infobar, Gtk::PACK_SHRINK);

	// create the buffer and set it for the TextView
	m_reftextbuffer = Gtk::TextBuffer::create();
	m_textview.set_buffer(m_reftextbuffer);

	// add the TreeView, inside a ScrolledWindow
	m_scrolledwindow.add(m_textview);

	// show the scrollbars only when they are necessary
	m_scrolledwindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	m_vbox.pack_start(m_scrolledwindow);

	// add button box
	m_vbox.pack_start(m_buttonbox, Gtk::PACK_SHRINK);

	m_buttonbox.pack_start(m_button_clear, Gtk::PACK_SHRINK);
	m_buttonbox.pack_start(m_button_quit, Gtk::PACK_SHRINK);
	m_buttonbox.set_spacing(6);
	m_buttonbox.set_layout(Gtk::BUTTONBOX_END);

	// connect signals
	m_infobar.signal_response().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_infobar_response));
	m_button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));
	m_button_clear.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_clear));
	m_reftextbuffer->signal_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_textbuffer_changed));

	show_all();

	// keep the InfoBar hidden until a message needs to be shown
	m_infobar.hide();

	// make the clear button insensitive until text is typed in the buffer
	// when the button is sensitive and it is pressed, the InfoBar is displayed with
	// a message
	m_button_clear.set_sensitive(false);
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_infobar_response(int)
{
	// clear the message and hide the InfoBar
	m_message_label.set_text("");
	m_infobar.hide();
}

void ExampleWindow::on_button_quit()
{
	hide();
}

void ExampleWindow::on_button_clear()
{
	m_reftextbuffer->set_text("");
	m_message_label.set_text("Clear the text");
	m_infobar.set_message_type(Gtk::MESSAGE_INFO);
	m_infobar.show();
}

void ExampleWindow::on_textbuffer_changed()
{
	m_button_clear.set_sensitive(m_reftextbuffer->size() > 0);
}
