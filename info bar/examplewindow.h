#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// signal handlers
		void on_infobar_response(int response);
		void on_button_quit();
		void on_button_clear();
		void on_textbuffer_changed();

		// child widgets
		Gtk::Box m_vbox;

		Gtk::ScrolledWindow m_scrolledwindow;
		Gtk::TextView m_textview;

		Glib::RefPtr<Gtk::TextBuffer> m_reftextbuffer;

		Gtk::InfoBar m_infobar;
		Gtk::Label m_message_label;

		Gtk::ButtonBox m_buttonbox;
		Gtk::Button m_button_quit, m_button_clear;
};

#endif // GTKMM_EXAMPLEWINDOW_H
