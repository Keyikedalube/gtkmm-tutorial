#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	m_vbox(Gtk::ORIENTATION_VERTICAL),
	m_button_close("Close")
{
	set_title("Gtk::Entry");

	add(m_vbox);

	m_entry.set_max_length(50);
	m_entry.set_text("Hello World");
	m_vbox.pack_start(m_entry, Gtk::PACK_SHRINK);

	m_entry.set_icon_from_icon_name("edit-find");
	m_entry.signal_icon_press().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_icon_pressed));

	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_close));
	m_vbox.pack_start(m_button_close, Gtk::PACK_SHRINK);
	m_button_close.set_can_default();
	m_button_close.grab_default();

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_icon_pressed(Gtk::EntryIconPosition /* icon pos */,
		const GdkEventButton* /* event */)
{
	std::cout << "Icon pressed." << std::endl;
}

void ExampleWindow::on_button_close()
{
	hide();
}
