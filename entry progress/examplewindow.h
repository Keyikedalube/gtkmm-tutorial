#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// signal handlers
		bool on_timeout();
		void on_button_close();

		// child widgets
		Gtk::Box m_vbox;
		Gtk::Entry m_entry;
		Gtk::Button m_button_close;
};

#endif // GTKMM_EXAMPLEWINDOW_H
