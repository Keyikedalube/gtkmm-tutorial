#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	m_vbox(Gtk::ORIENTATION_VERTICAL),
	m_button_close("Close")
{
	set_title("Gtk::Entry");

	add(m_vbox);

	m_entry.set_max_length(50);
	m_entry.set_text("Hello world");
	m_vbox.pack_start(m_entry, Gtk::PACK_SHRINK);

	// change the progress fraction every 0.1 second
	Glib::signal_timeout().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_timeout), 100);

	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_close));
	m_vbox.pack_start(m_button_close, Gtk::PACK_SHRINK);
	m_button_close.set_can_default();
	m_button_close.grab_default();

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

bool ExampleWindow::on_timeout()
{
	static double fraction = 0;
	m_entry.set_progress_fraction(fraction);

	fraction += 0.01;
	if (fraction > 1)
		fraction = 0;
	
	return true;
}

void ExampleWindow::on_button_close()
{
	hide();
}
