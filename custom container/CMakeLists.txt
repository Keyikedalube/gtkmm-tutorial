cmake_minimum_required(VERSION 3.15)

project(custom-container)

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})

set(EXAMPLE_CONTAINER_SOURCES
	main.cpp
	examplewindow.cpp
	mycontainer.cpp
)
add_executable(${PROJECT_NAME} ${EXAMPLE_CONTAINER_SOURCES})
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES})

