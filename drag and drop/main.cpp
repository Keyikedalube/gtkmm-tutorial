#include "dndwindow.h"
#include <gtkmm/application.h>

int main (int argc, char *argv[])
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  DnDWindow dndWindow;

  //Shows the window and returns when it is closed.
  return app->run(dndWindow);
}
