#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	m_vbox_top(Gtk::ORIENTATION_VERTICAL, 0),
	m_vbox2(Gtk::ORIENTATION_VERTICAL, 20),
	m_vbox_hscale(Gtk::ORIENTATION_VERTICAL, 10),
	m_hbox_scales(Gtk::ORIENTATION_HORIZONTAL, 10),
	m_hbox_combo(Gtk::ORIENTATION_HORIZONTAL, 10),
	m_hbox_digits(Gtk::ORIENTATION_HORIZONTAL, 10),
	m_hbox_pagesize(Gtk::ORIENTATION_HORIZONTAL, 10),

	// value, lower, upper, step_increment, page_increment, page_size
	// note that the page_size value only makes a difference for
	// scrollbar widgets, and the highest value you'll get is actually
	// (upper - page_size)
	m_adjustment(Gtk::Adjustment::create(0.0, 0.0, 101.0, 0.1, 1.0, 1.0)),
	m_adjustment_digits(Gtk::Adjustment::create(1.0, 0.0, 5.0, 1.0, 2.0)),
	m_adjustment_pagesize(Gtk::Adjustment::create(1.0, 1.0, 101.0)),

	m_vscale(m_adjustment, Gtk::ORIENTATION_VERTICAL),
	m_hscale(m_adjustment, Gtk::ORIENTATION_HORIZONTAL),
	m_scale_digits(m_adjustment_digits),
	m_scale_pagesize(m_adjustment_pagesize),

	// a checkbutton to control whether the value is displayed or not
	m_checkbutton("Display value on scale widgets", 0),

	// reuse te same adjustment again
	// notice how this causes the scales to always be updated
	// continuously when the scrollbar is moved
	m_scrollbar(m_adjustment),

	m_button_quit("quit")
{
	set_title("range controls");
	set_default_size(300, 350);

	// vscale
	m_vscale.set_digits(1);
	m_vscale.set_value_pos(Gtk::POS_TOP);
	m_vscale.set_draw_value();
	m_vscale.set_inverted();

	// hscale
	m_hscale.set_digits(1);
	m_hscale.set_value_pos(Gtk::POS_TOP);
	m_hscale.set_draw_value();

	add(m_vbox_top);
	m_vbox_top.pack_start(m_vbox2);
	m_vbox2.set_border_width(10);
	m_vbox2.pack_start(m_hbox_scales);

	// put vscale and hscale (above scrollbar) side-by-side
	m_hbox_scales.pack_start(m_vscale);
	m_hbox_scales.pack_start(m_vbox_hscale);

	m_vbox_hscale.pack_start(m_hscale);

	// scrollbar
	m_vbox_hscale.pack_start(m_scrollbar);

	// checkbutton
	m_checkbutton.set_active();
	m_checkbutton.signal_toggled().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbutton_toggled));
	m_vbox2.pack_start(m_checkbutton, Gtk::PACK_SHRINK);

	// position combobox
	// create the tree model
	m_refTreeModel = Gtk::ListStore::create(m_columns);
	m_combobox_position.set_model(m_refTreeModel);
	m_combobox_position.pack_start(m_columns.m_col_title);

	// fill the combobox's tree model
	Gtk::TreeModel::Row row = *(m_refTreeModel->append());
	row[m_columns.m_col_position_type] = Gtk::POS_TOP;
	row[m_columns.m_col_title] = "Top";
	row = *(m_refTreeModel->append());
	row[m_columns.m_col_position_type] = Gtk::POS_BOTTOM;
	row[m_columns.m_col_title] = "Bottom";
	row = *(m_refTreeModel->append());
	row[m_columns.m_col_position_type] = Gtk::POS_LEFT;
	row[m_columns.m_col_title] = "Left";
	row = *(m_refTreeModel->append());
	row[m_columns.m_col_position_type] = Gtk::POS_RIGHT;
	row[m_columns.m_col_title] = "Right";

	m_vbox2.pack_start(m_hbox_combo, Gtk::PACK_SHRINK);
	m_hbox_combo.pack_start(
			*Gtk::make_managed<Gtk::Label>("Scale Value Position: ", 0), Gtk::PACK_SHRINK);
	m_hbox_combo.pack_start(m_combobox_position);
	m_combobox_position.signal_changed().connect(sigc::mem_fun(*this, &ExampleWindow::on_combo_position));
	m_combobox_position.set_active(0); // top

	// digits
	m_hbox_digits.pack_start(
			*Gtk::make_managed<Gtk::Label>("Scale Digits: ", 0), Gtk::PACK_SHRINK);
	m_scale_digits.set_digits(0);
	m_adjustment_digits->signal_value_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_adjustment1_value_changed));
	m_hbox_digits.pack_start(m_scale_digits);

	// page size
	m_hbox_pagesize.pack_start(
			*Gtk::make_managed<Gtk::Label>("Scrollbar Page Size: ", 0),
			Gtk::PACK_SHRINK);
	m_scale_pagesize.set_digits(0);
	m_adjustment_pagesize->signal_value_changed().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_adjustment2_value_changed));
	m_hbox_pagesize.pack_start(m_scale_pagesize);

	m_vbox2.pack_start(m_hbox_digits, Gtk::PACK_SHRINK);
	m_vbox2.pack_start(m_hbox_pagesize, Gtk::PACK_SHRINK);
	m_vbox_top.pack_start(m_separator, Gtk::PACK_SHRINK);
	m_vbox_top.pack_start(m_button_quit, Gtk::PACK_SHRINK);

	m_button_quit.set_can_default();
	m_button_quit.grab_default();
	m_button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));
	m_button_quit.set_border_width(10);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_checkbutton_toggled()
{
	m_vscale.set_draw_value(m_checkbutton.get_active());
	m_hscale.set_draw_value(m_checkbutton.get_active());
}

void ExampleWindow::on_combo_position()
{
	Gtk::TreeModel::iterator iter = m_combobox_position.get_active();
	if (!iter)
		return;

	Gtk::TreeModel::Row row = *iter;
	if (!row)
		return;

	const Gtk::PositionType postype = row[m_columns.m_col_position_type];

	m_vscale.set_value_pos(postype);
	m_hscale.set_value_pos(postype);
}

void ExampleWindow::on_adjustment1_value_changed()
{
	const double val = m_adjustment_digits->get_value();
	m_vscale.set_digits((int)val);
	m_hscale.set_digits((int)val);
}

void ExampleWindow::on_adjustment2_value_changed()
{
	const double val = m_adjustment_pagesize->get_value();
	m_adjustment->set_page_size(val);
	m_adjustment->set_page_increment(val);

	// note that we don't have to emit the "changed" signal
	// because gtkmm does this for us
}

void ExampleWindow::on_button_quit()
{
	hide();
}
