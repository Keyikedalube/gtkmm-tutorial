#ifndef GTKMM_EXAMPLE_RANGEWIDGETS_H
#define GTKMM_EXAMPLE_RANGEWIDGETS_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// signal handlers
		void on_checkbutton_toggled();
		void on_combo_position();
		void on_adjustment1_value_changed();
		void on_adjustment2_value_changed();
		void on_button_quit();

		// child widgets
		Gtk::Box m_vbox_top, m_vbox2, m_vbox_hscale;
		Gtk::Box m_hbox_scales, m_hbox_combo, m_hbox_digits, m_hbox_pagesize;

		Glib::RefPtr<Gtk::Adjustment> m_adjustment, m_adjustment_digits, m_adjustment_pagesize;

		Gtk::Scale m_vscale;
		Gtk::Scale m_hscale, m_scale_digits, m_scale_pagesize;

		Gtk::Separator m_separator;

		Gtk::CheckButton m_checkbutton;

		Gtk::Scrollbar m_scrollbar;

		// tree model columns
		class ModelColumns: public Gtk::TreeModel::ColumnRecord
		{
		public:
			ModelColumns()
			{
				add(m_col_position_type);
				add(m_col_title);
			}

			Gtk::TreeModelColumn<Gtk::PositionType> m_col_position_type;
			Gtk::TreeModelColumn<Glib::ustring> m_col_title;
		};

		ModelColumns m_columns;

		// child widgets
		Gtk::ComboBox m_combobox_position;
		Glib::RefPtr<Gtk::ListStore> m_refTreeModel;

		Gtk::Button m_button_quit;
};

#endif // GTKMM_EXAMPLE_RANGEWIDGETS_H

