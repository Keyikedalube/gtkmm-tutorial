cmake_minimum_required(VERSION 3.15)

project(main-menu)

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})

# Our custom target implementation for compiling toolbar resources file
find_program(GLIB_COMPILE_RESOURCES NAMES glib-compile-resources REQUIRED)
mark_as_advanced(GLIB_COMPILE_RESOURCES)

set(GRESOURCE_C toolbar.gresource.c)
set(GRESOURCE_XML toolbar.gresource.xml)

add_custom_command(
	OUTPUT ${GRESOURCE_C}
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	COMMAND ${GLIB_COMPILE_RESOURCES}
	ARGS
		--generate-source
		--target=${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
		${GRESOURCE_XML}
	VERBATIM
	MAIN_DEPENDENCY ${GRESOURCE_XML}
	DEPENDS
		toolbar.glade
		rain.png
)

# Then set our custom target
add_custom_target(
	toolbar-resource
	DEPENDS ${GRESOURCE_C}
)

# Finally, build our project target
set(SOURCES
	main.cpp
	examplewindow.cpp
	${GRESOURCE_C}
	)
add_executable(${PROJECT_NAME} ${SOURCES})
add_dependencies(${PROJECT_NAME} toolbar-resource)
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES})

