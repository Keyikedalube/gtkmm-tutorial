#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	label1("Contents of tab 1"),
	label2("Contents of tab 2"),
	button_quit("Quit")
{
	set_title("Gtk::Notebook example");
	set_border_width(10);
	set_default_size(400, 200);

	add(v_box);

	notebook.set_border_width(10);
	v_box.pack_start(notebook);
	v_box.pack_start(button_box, Gtk::PACK_SHRINK);

	button_box.pack_start(button_quit, Gtk::PACK_SHRINK);
	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	notebook.append_page(label1, "First");
	notebook.append_page(label2, "Second");

	notebook.signal_switch_page().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_notebook_switch_page));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}

void ExampleWindow::on_notebook_switch_page(Gtk::Widget *page, guint page_num)
{
	std::cout << "Switched to tab with index " << page_num << std::endl;
}
