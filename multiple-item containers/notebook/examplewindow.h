#ifndef GTKMM_EXAMPLE_WINDOW_H
#define GTKMM_EXAMPLE_WINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_quit();
		void on_notebook_switch_page(Gtk::Widget *page, guint page_num);

		/* child widgets */
		Gtk::Box v_box;
		Gtk::Notebook notebook;
		Gtk::Label label1, label2;
		Gtk::ButtonBox button_box;
		Gtk::Button button_quit;
};

#endif // GTKMM_EXAMPLE_WINDOW_H
