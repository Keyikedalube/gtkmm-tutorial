#include <iostream>
#include "examplewindow.h"
#include "packbox.h"

ExampleWindow::ExampleWindow(int which):
	m_box1(Gtk::ORIENTATION_VERTICAL),
	m_button_quit("Quit")
{
	set_title("Gtk::Box example");

	PackBox *pack_box1, *pack_box2, *pack_box3, *pack_box4, *pack_box5;

	switch (which) {
		case 1:
			m_label1.set_text("Gtk::Box(Gtk::ORIENTATION_HORIZONTAL); set_homogoneous(false);");
			m_label1.set_halign(Gtk::ALIGN_START);
			m_label1.set_valign(Gtk::ALIGN_START);

			m_box1.pack_start(m_label1, Gtk::PACK_SHRINK);

			pack_box1 = Gtk::make_managed<PackBox>(false, 0, Gtk::PACK_SHRINK);
			m_box1.pack_start(*pack_box1, Gtk::PACK_SHRINK);

			pack_box2 = Gtk::make_managed<PackBox>(false, 0, Gtk::PACK_EXPAND_PADDING);
			m_box1.pack_start(*pack_box2, Gtk::PACK_SHRINK);

			pack_box3 = Gtk::make_managed<PackBox>(false, 0, Gtk::PACK_EXPAND_WIDGET);
			m_box1.pack_start(*pack_box3, Gtk::PACK_SHRINK);

			m_box1.pack_start(m_separator1, Gtk::PACK_SHRINK, 5);

			m_label2.set_text("Gtk::Box(Gtk::ORIENTATION_HORIZONTAL); set_homogoneous(true);");
			m_label2.set_halign(Gtk::ALIGN_START);
			m_label2.set_valign(Gtk::ALIGN_START);
			m_box1.pack_start(m_label2, Gtk::PACK_SHRINK);

			pack_box4 = Gtk::make_managed<PackBox>(true, 0, Gtk::PACK_EXPAND_PADDING);
			m_box1.pack_start(*pack_box4, Gtk::PACK_SHRINK);

			pack_box5 = Gtk::make_managed<PackBox>(true, 0, Gtk::PACK_EXPAND_WIDGET);
			m_box1.pack_start(*pack_box5, Gtk::PACK_SHRINK);

			m_box1.pack_start(m_separator2, Gtk::PACK_SHRINK, 5);

			break;

		case 2:
			m_label1.set_text("Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10); set_homogoneous(false);");
			m_label1.set_halign(Gtk::ALIGN_START);
			m_label1.set_valign(Gtk::ALIGN_START);
			m_box1.pack_start(m_label1, Gtk::PACK_SHRINK);

			pack_box1 = Gtk::make_managed<PackBox>(false, 10, Gtk::PACK_EXPAND_PADDING);
			m_box1.pack_start(*pack_box1, Gtk::PACK_SHRINK);

			pack_box2 = Gtk::make_managed<PackBox>(false, 10, Gtk::PACK_EXPAND_WIDGET);
			m_box1.pack_start(*pack_box2, Gtk::PACK_SHRINK);

			m_box1.pack_start(m_separator1, Gtk::PACK_SHRINK, 5);

			m_label2.set_text("Gtk::Box(Gtk::ORIENTATION_HORIZONTAL); set_homogoneous(false);");
			m_label2.set_halign(Gtk::ALIGN_START);
			m_label2.set_valign(Gtk::ALIGN_START);
			m_box1.pack_start(m_label2, Gtk::PACK_SHRINK);

			pack_box3 = Gtk::make_managed<PackBox>(false, 0, Gtk::PACK_SHRINK, 10);
			m_box1.pack_start(*pack_box3, Gtk::PACK_SHRINK);

			pack_box4 = Gtk::make_managed<PackBox>(false, 0, Gtk::PACK_EXPAND_WIDGET, 10);
			m_box1.pack_start(*pack_box4, Gtk::PACK_SHRINK);

			m_box1.pack_start(m_separator2, Gtk::PACK_SHRINK, 5);

			break;

		case 3:
			pack_box1 = Gtk::make_managed<PackBox>(false, 0, Gtk::PACK_SHRINK);

			m_label1.set_text("end");

			pack_box1->pack_end(m_label1, Gtk::PACK_SHRINK);

			m_box1.pack_start(*pack_box1, Gtk::PACK_SHRINK);

			/* this explicitly sets the separator to 500 pixels wide by 5 pixels
			 * high. This is so the hbox we created will also be 500 pixels wide,
			 * and the "end" label will be separated from the other labels in the
			 * hbox. Otherwise, all the widgets in the hbox would be packed as
			 * close together as possible
			 */
			m_separator1.set_size_request(500, 5);

			m_box1.pack_start(m_separator1, Gtk::PACK_SHRINK, 5);

			break;

		default:
			std::cerr << "Unexpected command-line option" << std::endl;
			break;
	}

	m_button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit_clicked));

	m_box_quit.pack_start(m_button_quit, Gtk::PACK_EXPAND_PADDING);
	m_box1.pack_start(m_box_quit, Gtk::PACK_SHRINK);

	add(m_box1);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit_clicked()
{
	hide();
}
