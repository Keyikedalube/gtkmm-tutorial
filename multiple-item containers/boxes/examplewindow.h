#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow(int which);
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_quit_clicked();

		/* child widgets */
		Gtk::Button m_button;
		Gtk::Box m_box1;
		Gtk::Box m_box_quit;
		Gtk::Button m_button_quit;
		Gtk::Label m_label1, m_label2;
		Gtk::Separator m_separator1, m_separator2;
};

#endif // GKTMM_EXAMPLEWINDOW_H
