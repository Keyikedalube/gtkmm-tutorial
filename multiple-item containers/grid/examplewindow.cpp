#include <iostream>
#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	button1("Button 1"),
	button2("Button 2"),
	button_quit("Quit")
{
	set_title("Gtk::Grid");
	set_border_width(12);

	add(grid);

	grid.add(button1);
	grid.add(button2);
	grid.attach_next_to(button_quit, button1, Gtk::POS_BOTTOM, 2, 1);

	button1.signal_clicked().connect(sigc::bind<Glib::ustring>(sigc::mem_fun(*this,
					&ExampleWindow::on_button_numbered), "Button 1"));
	button2.signal_clicked().connect(sigc::bind<Glib::ustring>(sigc::mem_fun(*this,
					&ExampleWindow::on_button_numbered), "Button 2"));

	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}

void ExampleWindow::on_button_numbered(const Glib::ustring &data)
{
	std::cout << data << " was pressed" << std::endl;
}
