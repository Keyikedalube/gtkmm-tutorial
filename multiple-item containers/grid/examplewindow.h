#ifndef GTKMM_EXAMPLE_WINDOW_H
#define GTKMM_EXAMPLE_WINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	private:
		/* signal handlers */
		void on_button_quit();
		void on_button_numbered(const Glib::ustring &data);

		/* child widgets */
		Gtk::Grid grid;
		Gtk::Button button1, button2, button_quit;
};

#endif // GTKMM_EXAMPLE_WINDOW_H
