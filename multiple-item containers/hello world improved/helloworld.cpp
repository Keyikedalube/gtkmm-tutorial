#include "helloworld.h"
#include <iostream>

HelloWorld::HelloWorld():
	m_button1("Button 1"),
	m_button2("Button 2")
{
	set_title("Hello Buttons!");
	set_border_width(10);

	add(m_box1);

	m_button1.signal_clicked().connect(sigc::bind<Glib::ustring>(
				sigc::mem_fun(*this, &HelloWorld::on_button_clicked), "button 1"));

	m_box1.pack_start(m_button1);

	m_button1.show();

	m_button2.signal_clicked().connect(sigc::bind<-1, Glib::ustring>(
				sigc::mem_fun(*this, &HelloWorld::on_button_clicked), "button 2"));

	m_box1.pack_start(m_button2);

	m_button2.show();

	m_box1.show();
}

HelloWorld::~HelloWorld()
{
}

void HelloWorld::on_button_clicked(Glib::ustring data)
{
	std::cout << "Hello World - " << data << " was pressed" << std::endl;
}
