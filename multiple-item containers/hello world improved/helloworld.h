#ifndef GTKMM_EXAMPLE_HELLOWORLD_H
#define GTKMM_EXAMPLE_HELLOWORLD_H

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>

class HelloWorld: public Gtk::Window {
	public:
		HelloWorld();
		virtual ~HelloWorld();

	protected:
		/* signal handlers */
		void on_button_clicked(Glib::ustring data);

		/* child widgets */
		Gtk::Box m_box1;
		Gtk::Button m_button1, m_button2;
};

#endif // GTKMM_EXAMPLE_HELLOWORLD_H
