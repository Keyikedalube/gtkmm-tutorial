#include "examplewindow.h"
#include "examplebuttonbox.h"

ExampleWindow::ExampleWindow():
	vbox_main(Gtk::ORIENTATION_VERTICAL),
	vbox(Gtk::ORIENTATION_VERTICAL),
	frame_horizontal("Horizontal Button Boxes"),
	frame_vertical("Vertical Button Boxes"),
	frame_vertical_exit("Exit program"),
	exit_button("Exit program")
{
	set_title("Gtk::ButtonBox");
	add(vbox_main);

	vbox_main.pack_start(frame_horizontal, Gtk::PACK_EXPAND_WIDGET, 10);

	/* The horizontal ButtonBoxes */
	vbox.set_border_width(10);
	frame_horizontal.add(vbox);

	vbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(true, "Spread (spacing 40)", 40, Gtk::BUTTONBOX_SPREAD),
			Gtk::PACK_EXPAND_WIDGET);

	vbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(true, "Edge (spacing 30)", 30, Gtk::BUTTONBOX_EDGE),
			Gtk::PACK_EXPAND_WIDGET, 5);

	vbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(true, "Start (spacing 20)", 20, Gtk::BUTTONBOX_START),
			Gtk::PACK_EXPAND_WIDGET, 5);

	vbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(true, "End (spacing 10)", 10, Gtk::BUTTONBOX_END),
			Gtk::PACK_EXPAND_WIDGET, 5);


	/* The vertical ButtonBoxes */
	vbox_main.pack_start(frame_vertical, Gtk::PACK_EXPAND_WIDGET, 10);

	hbox.set_border_width(10);
	frame_vertical.add(hbox);

	hbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(false, "Spread (spacing 5)", 5, Gtk::BUTTONBOX_SPREAD),
			Gtk::PACK_EXPAND_WIDGET);

	hbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(false, "Edge (spacing 30)", 30, Gtk::BUTTONBOX_EDGE),
			Gtk::PACK_EXPAND_WIDGET, 5);

	hbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(false, "Start (spacing 20)", 20, Gtk::BUTTONBOX_START),
			Gtk::PACK_EXPAND_WIDGET, 5);

	hbox.pack_start(*Gtk::make_managed<ExampleButtonBox>
			(false, "End (spacing 10)", 10, Gtk::BUTTONBOX_END),
			Gtk::PACK_EXPAND_WIDGET, 5);

	/* Exit program button */
	vbox_main.pack_start(frame_vertical_exit, Gtk::PACK_EXPAND_WIDGET, 20);

	frame_vertical_exit.add(exit_button);
	exit_button.signal_clicked().connect(sigc::mem_fun(*this, &ExampleWindow::on_button_clicked));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_clicked()
{
	hide();
}
