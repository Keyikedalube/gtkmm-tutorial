#ifndef GTKMM_EXAMPLE_BUTTONBOX_H
#define GTKMM_EXAMPLE_BUTTONBOX_H

#include <gtkmm.h>

class ExampleButtonBox: public Gtk::Frame {
	public:
		ExampleButtonBox(
				bool horizontal,
				const Glib::ustring &title,
				gint spacing,
				Gtk::ButtonBoxStyle layout);

	protected:
		Gtk::Button button_ok, button_cancel, button_help;
};

#endif // GTKMM_EXAMPLE_BUTTONBOX_H
