#include "examplebuttonbox.h"

ExampleButtonBox::ExampleButtonBox(
		bool horizontal,
		const Glib::ustring &title,
		gint spacing,
		Gtk::ButtonBoxStyle layout):
	Gtk::Frame(title),
	button_ok("Ok"),
	button_cancel("Cancel"),
	button_help("Help")
{
	Gtk::ButtonBox *box = nullptr;

	if (horizontal)
		box = Gtk::make_managed<Gtk::ButtonBox>(Gtk::ORIENTATION_HORIZONTAL);
	else
		box = Gtk::make_managed<Gtk::ButtonBox>(Gtk::ORIENTATION_VERTICAL);

	box->set_border_width(5);

	add(*box);

	box->set_layout(layout);
	box->set_spacing(spacing);

	box->add(button_ok);
	box->add(button_cancel);
	box->add(button_help);
}
