#ifndef GTKMM_EXAMPLE_WINDOW_H
#define GTKMM_EXAMPLE_WINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_clicked();

		/* child widgets */
		Gtk::Box vbox_main, vbox;
		Gtk::Box hbox;
		Gtk::Frame frame_horizontal, frame_vertical;
		Gtk::Frame frame_vertical_exit;
		Gtk::Button exit_button;
};

#endif // GTKMM_EXAMPLE_WINDOW_H
