#ifndef GTKMM_EXAMPLE_WINDOW_H
#define GTKMM_EXAMPLE_WINDOW_H

#include "exampleassistant.h"
#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	private:
		/* signal handlers */
		void on_button_clicked();
		void on_assistant_apply();

		/* child widgets */
		Gtk::Grid grid;
		Gtk::Button button;
		Gtk::Label label1, label2;
		Gtk::CheckButton check;
		Gtk::Entry entry;
		ExampleAssistant assistant;
};

#endif // GTKMM_EXAMPLE_WINDOW_H
