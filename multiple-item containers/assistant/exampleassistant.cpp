#include "exampleassistant.h"
#include <iostream>

ExampleAssistant::ExampleAssistant():
	box(Gtk::ORIENTATION_HORIZONTAL, 12),
	label1("Type text to allow the assistant to continue:"),
	label2("Confirmation page"),
	check_button("Optional extra information")
{
	set_title("Gtk::Assistant example");
	set_border_width(12);
	set_default_size(400, 300);

	box.pack_start(label1);
	box.pack_start(entry);

	append_page(box);
	append_page(check_button);
	append_page(label2);

	set_page_title(*get_nth_page(0), "Page 1");
	set_page_title(*get_nth_page(1), "Page 2");
	set_page_title(*get_nth_page(2), "Confirmation");

	set_page_complete(check_button, true);
	set_page_complete(label2, true);

	set_page_type(box, Gtk::ASSISTANT_PAGE_INTRO);
	set_page_type(label2, Gtk::ASSISTANT_PAGE_CONFIRM);

	signal_apply().connect(sigc::mem_fun(*this,
				&ExampleAssistant::on_assistant_apply));
	signal_cancel().connect(sigc::mem_fun(*this,
				&ExampleAssistant::on_assistant_cancel));
	signal_close().connect(sigc::mem_fun(*this,
				&ExampleAssistant::on_assistant_close));
	signal_prepare().connect(sigc::mem_fun(*this,
				&ExampleAssistant::on_assistant_prepare));

	entry.signal_changed().connect(sigc::mem_fun(*this,
				&ExampleAssistant::on_entry_changed));

	show_all_children();
}

ExampleAssistant::~ExampleAssistant()
{
}

void ExampleAssistant::get_result(bool &check_button_state, Glib::ustring &entry_text)
{
	check_button_state = check_button.get_active();
	entry_text = entry.get_text();
}

void ExampleAssistant::on_assistant_apply()
{
	std::cout << "Apply was clicked";
	print_status();
}

void ExampleAssistant::on_assistant_cancel()
{
	std::cout << "Cancel was clicked";
	print_status();
	hide();
}

void ExampleAssistant::on_assistant_close()
{
	std::cout << "Assistant was closed";
	print_status();
	hide();
}

void ExampleAssistant::on_assistant_prepare(Gtk::Widget *)
{
	set_title(Glib::ustring::compose("Gtk::Assistant example (Page %1 of %2)",
				get_current_page() + 1, get_n_pages()));
}

void ExampleAssistant::on_entry_changed()
{
	/* the page is only complete if the entry contains text */
	if (entry.get_text_length())
		set_page_complete(box, true);
	else
		set_page_complete(box, false);
}

void ExampleAssistant::print_status()
{
	std::cout << ", entry contents: \"" << entry.get_text()
		<< "\", check button status: " << check_button.get_active()
		<< std::endl;
}
