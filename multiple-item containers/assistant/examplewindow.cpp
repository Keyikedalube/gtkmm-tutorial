#include "examplewindow.h"

ExampleWindow::ExampleWindow():
	button("Show the assistant"),
	label1("State of assistant check button:", Gtk::ALIGN_START, Gtk::ALIGN_CENTER),
	label2("Contents of assistant entry:", Gtk::ALIGN_START, Gtk::ALIGN_CENTER)
{
	set_title("Gtk::Assistant example");
	set_border_width(12);

	grid.set_row_homogeneous(true);

	grid.attach(button, 0, 0, 2, 1);
	button.set_hexpand(true);
	button.set_valign(Gtk::ALIGN_CENTER);

	grid.attach(label1, 0, 1, 1, 1);
	grid.attach(label2, 0, 2, 1, 1);

	grid.attach(check, 1, 1, 1, 1);
	check.set_halign(Gtk::ALIGN_START);

	grid.attach(entry, 1, 2, 1, 1);
	entry.set_hexpand(true);

	add(grid);

	button.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_clicked));
	assistant.signal_apply().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_assistant_apply));

	check.set_sensitive(false);
	entry.set_sensitive(false);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_assistant_apply()
{
	bool check_state;
	Glib::ustring entry_text;

	assistant.get_result(check_state, entry_text);
	check.set_active(check_state);
	entry.set_text(entry_text);
}

void ExampleWindow::on_button_clicked()
{
	assistant.show();
}
