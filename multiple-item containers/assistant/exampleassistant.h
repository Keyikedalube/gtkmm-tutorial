#ifndef GTKMM_EXAMPLE_ASSISTANT_H
#define GTKMM_EXAMPLE_ASSISTANT_H

#include <gtkmm.h>

class ExampleAssistant: public Gtk::Assistant {
	public:
		ExampleAssistant();
		virtual ~ExampleAssistant();

		void get_result(bool &check_state, Glib::ustring &entry_text);

	private:
		/* signal handlers */
		void on_assistant_apply();
		void on_assistant_cancel();
		void on_assistant_close();
		void on_assistant_prepare(Gtk::Widget *widget);
		void on_entry_changed();

		/* member function */
		void print_status();

		/* child widgets */
		Gtk::Box box;
		Gtk::Label label1, label2;
		Gtk::CheckButton check_button;
		Gtk::Entry entry;
};

#endif // GTKMM_EXAMPLE_ASSISTANT_H
