#include "helloworld.h"
#include <iostream>

HelloWorld::HelloWorld()
	: m_button("Hello World") // creates a new button with label "Hello World"
{
	// sets the border width of the window
	set_border_width(10);
	
	// when the button receives the "clicked" signal, it will call the
	// on_button_clicked() method defined below
	m_button.signal_clicked().connect(sigc::mem_fun(*this, &HelloWorld::on_button_clicked));

	// this packs the button into the window (a container)
	add(m_button);

	// the final step is to display this newly created widget
	m_button.show();
}

HelloWorld::~HelloWorld()
{
}

void HelloWorld::on_button_clicked()
{
	std::cout << "Hello World" << std::endl;
}
