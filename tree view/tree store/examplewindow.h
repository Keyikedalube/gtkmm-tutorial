#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_quit();
		void on_tree_view_row_activated(const Gtk::TreeModel::Path &path, Gtk::TreeViewColumn *column);

		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
				}

				Gtk::TreeModelColumn<int> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
		};

		ModelColumns columns;

		/* child widgets */
		Gtk::Box v_box;

		Gtk::ScrolledWindow scrolled_window;
		Gtk::TreeView tree_view;
		Glib::RefPtr<Gtk::TreeStore> ref_tree_model;

		Gtk::ButtonBox button_box;
		Gtk::Button button_quit;
};

#endif // GTKMM_EXAMPLEWINDOW_H
