#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	button_quit("Quit")
{
	set_title("Gtk::TreeView (TreeStore) example");
	set_border_width(5);
	set_default_size(400, 200);

	add(v_box);

	scrolled_window.add(tree_view);
	scrolled_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	v_box.pack_start(scrolled_window);
	v_box.pack_start(button_box, Gtk::PACK_SHRINK);

	button_box.pack_start(button_quit, Gtk::PACK_SHRINK);
	button_box.set_border_width(5);
	button_box.set_layout(Gtk::BUTTONBOX_END);
	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	/* create the tree model */
	ref_tree_model = Gtk::TreeStore::create(columns);
	tree_view.set_model(ref_tree_model);

	/* all the items to be reordered with drag and drop */
	tree_view.set_reorderable();

	/* fill the TreeView's model */
	Gtk::TreeModel::Row row = *(ref_tree_model->append());
	row[columns.col_id] = 1;
	row[columns.col_name] = "Billy Bob";
	
	Gtk::TreeModel::Row child_row = *(ref_tree_model->append(row.children()));
	child_row[columns.col_id] = 11;
	child_row[columns.col_name] = "Billy Bob Junior";
	
	child_row = *(ref_tree_model->append(row.children()));
	child_row[columns.col_id] = 12;
	child_row[columns.col_name] = "Sue Bob";
	
	row = *(ref_tree_model->append());
	row[columns.col_id] = 2;
	row[columns.col_name] = "Joey Jojo";
	
	row = *(ref_tree_model->append());
	row[columns.col_id] = 3;
	row[columns.col_name] = "Rob McRoberts";
	
	child_row = *(ref_tree_model->append(row.children()));
	child_row[columns.col_id] = 31;
	child_row[columns.col_name] = "Xavier McRoberts";

	/* add the TreeView's view columns */
	tree_view.append_column("ID", columns.col_id);
	tree_view.append_column("Name", columns.col_name);

	tree_view.signal_row_activated().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_tree_view_row_activated));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}

void ExampleWindow::on_tree_view_row_activated(const Gtk::TreeModel::Path &path, Gtk::TreeViewColumn *column)
{
	Gtk::TreeModel::iterator iter = ref_tree_model->get_iter(path);
	if (iter) {
		Gtk::TreeModel::Row row = *iter;
		std::cout << "Row activated: ID = " << row[columns.col_id] << ", Name = "
			<< row[columns.col_name] << std::endl;
	}
}
