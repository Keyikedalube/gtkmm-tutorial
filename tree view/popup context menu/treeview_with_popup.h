#ifndef GTKMM_TREEVIEW_WITH_POPUP_H
#define GTKMM_TREEVIEW_WITH_POPUP_H

#include <gtkmm.h>

class TreeViewWithPopup: public Gtk::TreeView {
	public:
		TreeViewWithPopup();
		virtual ~TreeViewWithPopup();

	protected:
		/* override signal handler */
		bool on_button_press_event(GdkEventButton *button_event) override;

		/* signal handler for popup menu items */
		void on_menu_file_popup_generic();

		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
				}

				Gtk::TreeModelColumn<unsigned int> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
		};

		ModelColumns columns;

		Glib::RefPtr<Gtk::ListStore> ref_treemodel;

		Gtk::Menu menu_popup;
};

#endif // GTKMM_TREEVIEW_WITH_POPUP_H
