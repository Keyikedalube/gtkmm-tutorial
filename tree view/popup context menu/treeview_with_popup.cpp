#include "treeview_with_popup.h"
#include <iostream>

TreeViewWithPopup::TreeViewWithPopup()
{
	ref_treemodel = Gtk::ListStore::create(columns);
	set_model(ref_treemodel);

	Gtk::TreeModel::Row row = *(ref_treemodel->append());
	row[columns.col_id] = 1;
	row[columns.col_name] = "right-click on this";

	row = *(ref_treemodel->append());
	row[columns.col_id] = 2;
	row[columns.col_name] = "or this";

	row = *(ref_treemodel->append());
	row[columns.col_id] = 3;
	row[columns.col_name] = "or this, for a popup context menu";

	append_column("ID", columns.col_id);
	append_column("Name", columns.col_name);

	/* fill popup menu */
	auto item = Gtk::make_managed<Gtk::MenuItem>("_Edit", true);
	item->signal_activate().connect(sigc::mem_fun(*this,
				&TreeViewWithPopup::on_menu_file_popup_generic));
	menu_popup.append(*item);

	item = Gtk::make_managed<Gtk::MenuItem>("_Process", true);
	item->signal_activate().connect(sigc::mem_fun(*this,
				&TreeViewWithPopup::on_menu_file_popup_generic));
	menu_popup.append(*item);

	item = Gtk::make_managed<Gtk::MenuItem>("_Remove", true);
	item->signal_activate().connect(sigc::mem_fun(*this,
				&TreeViewWithPopup::on_menu_file_popup_generic));
	menu_popup.append(*item);

	menu_popup.accelerate(*this);
	menu_popup.show_all();
}

TreeViewWithPopup::~TreeViewWithPopup()
{
}

bool TreeViewWithPopup::on_button_press_event(GdkEventButton *button_event)
{
	bool return_value = false;

	/* call base class, to allow normal handling
	 * such as allowing the row to be selected by the right-click
	 */
	return_value = TreeView::on_button_press_event(button_event);

	/* do our custom stuff */
	if ((button_event->type == GDK_BUTTON_PRESS) && (button_event->button == 3))
		menu_popup.popup_at_pointer((GdkEvent *)button_event);

	return false;
}

void TreeViewWithPopup::on_menu_file_popup_generic()
{
	std::cout << "A popup menu item was selected" << std::endl;

	auto ref_selection = get_selection();
	if (ref_selection) {
		Gtk::TreeModel::iterator iter = ref_selection->get_selected();
		if (iter) {
			int id = (*iter)[columns.col_id];
			std::cout << " Selected ID = " << id << std::endl;
		}
	}
}

