#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	button_quit("Quit")
{
	set_title("Gtk::TreeView (ListStore) example");
	set_border_width(5);
	set_default_size(400, 200);

	add(v_box);

	scrolledwindow.add(treeview);
	scrolledwindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	v_box.pack_start(scrolledwindow);
	v_box.pack_start(buttonbox, Gtk::PACK_SHRINK);

	buttonbox.pack_start(button_quit, Gtk::PACK_SHRINK);
	buttonbox.set_border_width(5);
	buttonbox.set_layout(Gtk::BUTTONBOX_END);
	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}
