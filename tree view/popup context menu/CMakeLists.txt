cmake_minimum_required(VERSION 3.15)

project(popup-context-menu)

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
include_directories(${GTKMM_INCLUDE_DIRS})
link_directories(${GTKMM_LIBRARY_DIRS})

add_executable(${PROJECT_NAME} main.cpp examplewindow.cpp treeview_with_popup.cpp)
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES})

