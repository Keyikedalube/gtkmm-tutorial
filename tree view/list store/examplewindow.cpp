#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	button_quit("Quit")
{
	set_title("Gtk::TreeView (ListStore) exampe");
	set_border_width(5);
	set_default_size(400, 200);

	add(v_box);

	scrolled_window.add(tree_view);
	scrolled_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	v_box.pack_start(scrolled_window);
	v_box.pack_start(button_box, Gtk::PACK_SHRINK);

	button_box.pack_start(button_quit, Gtk::PACK_SHRINK);
	button_box.set_border_width(5);
	button_box.set_layout(Gtk::BUTTONBOX_END);
	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	/* create the tree model */
	ref_tree_model = Gtk::ListStore::create(columns);
	tree_view.set_model(ref_tree_model);

	/* fill the TreeView's model */
	Gtk::TreeModel::Row row = *(ref_tree_model->append());
	row[columns.col_id] = 1;
	row[columns.col_name] = "Billy Box";
	row[columns.col_no] = 10;
	row[columns.col_per] = 15;

	row = *(ref_tree_model->append());
	row[columns.col_id] = 2;
	row[columns.col_name] = "Joey Jojo";
	row[columns.col_no] = 20;
	row[columns.col_per] = 55;

	row = *(ref_tree_model->append());
	row[columns.col_id] = 3;
	row[columns.col_name] = "Rob McRoberts";
	row[columns.col_no] = 30;
	row[columns.col_per] = 75;

	row = *(ref_tree_model->append());
	row[columns.col_id] = 4;
	row[columns.col_name] = "Stewey LaMach";
	row[columns.col_no] = 40;
	row[columns.col_per] = 25;

	/* add the TreeView's view columns
	 * this number will be shown with the default numeric formatting
	 */
	tree_view.append_column("ID", columns.col_id);
	tree_view.append_column("Name", columns.col_name);

	tree_view.append_column_numeric("Formatted number", columns.col_no,
			"%010d" /* 10 digits, using leading zeroes */);

	/* display a progress bar instead of a decimal number */
	auto cell = Gtk::make_managed<Gtk::CellRendererProgress>();
	int cols_count = tree_view.append_column("Some percentage", *cell);
	auto per_column = tree_view.get_column(cols_count - 1);
	if (per_column)
		per_column->add_attribute(cell->property_value(), columns.col_per);

	/* make all the columns reorderable */
	for (guint i = 0; i < 2; i++) {
		auto column = tree_view.get_column(i);
		column->set_reorderable();
	}

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}
