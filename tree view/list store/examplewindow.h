#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_quit();

		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
					add(col_no);
					add(col_per);
				}

				Gtk::TreeModelColumn<unsigned int> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
				Gtk::TreeModelColumn<short> col_no;
				Gtk::TreeModelColumn<int> col_per;
		};

		ModelColumns columns;

		/* child widgets */
		Gtk::Box v_box;

		Gtk::ScrolledWindow scrolled_window;
		Gtk::TreeView tree_view;
		Glib::RefPtr<Gtk::ListStore> ref_tree_model;

		Gtk::ButtonBox button_box;
		Gtk::Button button_quit;
};

#endif // GTKMM_EXAMPLEWINDOW_H
