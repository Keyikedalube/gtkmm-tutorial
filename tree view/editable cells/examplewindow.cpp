#include "examplewindow.h"
#include <iostream>
#include <cstdio>

using std::sprintf;
using std::strtol;

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	button_quit("Quit"),
	validate_retry(false)
{
	set_title("Gtk::TreeView Editable Cells example");
	set_border_width(5);
	set_default_size(400, 200);

	add(v_box);

	scrolledwindow.add(treeview);
	scrolledwindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	v_box.pack_start(scrolledwindow);
	v_box.pack_start(buttonbox, Gtk::PACK_SHRINK);

	buttonbox.pack_start(button_quit, Gtk::PACK_SHRINK);
	buttonbox.set_border_width(5);
	buttonbox.set_layout(Gtk::BUTTONBOX_END);
	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	/* create the tree model */
	ref_treemodel = Gtk::ListStore::create(columns);
	treeview.set_model(ref_treemodel);

	/* fill the TreeView's model */
	Gtk::TreeModel::Row row = *(ref_treemodel->append());
	row[columns.col_id] = 1;
	row[columns.col_name] = "Billy Bob";
	row[columns.col_foo] = true;
	row[columns.col_no] = 10;

	row = *(ref_treemodel->append());
	row[columns.col_id] = 2;
	row[columns.col_name] = "Joey Jojo";
	row[columns.col_foo] = true;
	row[columns.col_no] = 20;

	row = *(ref_treemodel->append());
	row[columns.col_id] = 3;
	row[columns.col_name] = "Rob McRoberts";
	row[columns.col_foo] = false;
	row[columns.col_no] = 30;

	/* add the TreeView's view columns
	 * we use the *_editable convenience methods for most of these
	 * because the default functionality is enough
	 */
	treeview.append_column_editable("ID", columns.col_id);
	treeview.append_column_editable("Name", columns.col_name);
	treeview.append_column_editable("foo", columns.col_foo);
	treeview.append_column_numeric_editable("foo", columns.col_no, "%010d");

	/* for this column, we create the CellRenderer ourselves, and connect our own
	 * signal handlers, so that we can validate the data that the user enters, and
	 * control how it is displayed
	 */
	treeviewcolumn_validated.set_title("validated (<10)");
	treeviewcolumn_validated.pack_start(cellrenderer_validated);
	treeview.append_column(treeviewcolumn_validated);

	/* tell the view column how to render the model values */
	treeviewcolumn_validated.set_cell_data_func(cellrenderer_validated, sigc::mem_fun(*this,
					&ExampleWindow::treeviewcolumn_validated_on_cell_data));

	/* make the CellRenderer editable, and handle its editing signals */
	cellrenderer_validated.property_editable() = true;

	cellrenderer_validated.signal_editing_started().connect(sigc::mem_fun(*this,
				&ExampleWindow::cellrenderer_validated_on_editing_started));

	cellrenderer_validated.signal_edited().connect(sigc::mem_fun(*this,
				&ExampleWindow::cellrenderer_validated_on_edited));

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}

void ExampleWindow::treeviewcolumn_validated_on_cell_data(
		Gtk::CellRenderer *renderer,
		const Gtk::TreeModel::iterator &iter)
{
	/* get the value from the model and show it appropriately in the view */
	if (iter) {
		Gtk::TreeModel::Row row = *iter;
		int model_value = row[columns.col_no_validated];

		/* this is just an example
		 * in this case, it would be easier to use append_column_editable() or
		 * append_column_numeric_editable()
		 */
		char buffer[32];
		sprintf(buffer, "%d", model_value);

		Glib::ustring view_text = buffer;
		cellrenderer_validated.property_text() = view_text;
	}
}

void ExampleWindow::cellrenderer_validated_on_editing_started(
		Gtk::CellEditable *celleditable, const Glib::ustring &path)
{
	/* start editing with previously-entered (but invalid) text,
	 * if we are allowing the use to correct some invalid data
	 */
	if (validate_retry) {
		/* this is the CellEditable inside the CellRenderer */
		auto celleditable_validated = celleditable;

		/* it's usually an Entry, at least for a CellRendererText */
		auto entry = dynamic_cast<Gtk::Entry *>(celleditable_validated);
		if (entry) {
			entry->set_text(invalid_text_for_retry);
			validate_retry = false;
			invalid_text_for_retry.clear();
		}
	}
}

void ExampleWindow::cellrenderer_validated_on_edited(
		const Glib::ustring &path_string,
		const Glib::ustring &new_text)
{
	Gtk::TreePath path(path_string);

	/* convert the inputed text to an integer, as needed by out model column */
	char *char_end = nullptr;
	int new_value = strtol(new_text.c_str(), &char_end, 10);

	if (new_value > 10) {
		/* prevent of numbers higher that 10 */
		Gtk::MessageDialog dialog(*this,
				"The number must be less than 10. Please try again.",
				false, Gtk::MESSAGE_ERROR);
		dialog.run();

		/* set the text to be used in the start_editing signal handler */
		invalid_text_for_retry = new_text;
		validate_retry = true;

		/* start editing again */
		treeview.set_cursor(path, treeviewcolumn_validated,
				cellrenderer_validated, true);
	} else {
		/* get the row from the path */
		Gtk::TreeModel::iterator iter = ref_treemodel->get_iter(path);
		if (iter) {
			Gtk::TreeModel::Row row = *iter;

			/* put the new value in the model */
			row[columns.col_no_validated] = new_value;
		}
	}
}
