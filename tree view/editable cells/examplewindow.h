#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_quit();

		void treeviewcolumn_validated_on_cell_data(Gtk::CellRenderer *, const Gtk::TreeModel::iterator &);
		void cellrenderer_validated_on_editing_started(Gtk::CellEditable *, const Glib::ustring &);
		void cellrenderer_validated_on_edited(const Glib::ustring &, const Glib::ustring &);

		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
					add(col_foo);
					add(col_no);
					add(col_no_validated);
				}

				Gtk::TreeModelColumn<unsigned int> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
				Gtk::TreeModelColumn<bool> col_foo;
				Gtk::TreeModelColumn<int> col_no;
				Gtk::TreeModelColumn<int> col_no_validated;
		};

		ModelColumns columns;

		/* child widgets */
		Gtk::Box v_box;

		Gtk::ScrolledWindow scrolledwindow;
		Gtk::TreeView treeview;
		Glib::RefPtr<Gtk::ListStore> ref_treemodel;

		Gtk::ButtonBox buttonbox;
		Gtk::Button button_quit;

		/* for the validated column */
		Gtk::CellRendererText cellrenderer_validated;
		Gtk::TreeView::Column treeviewcolumn_validated;
		bool validate_retry;
		Glib::ustring invalid_text_for_retry;
};

#endif // GTKMM_EXAMPLEWINDOW_H
