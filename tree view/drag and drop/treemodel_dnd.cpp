#include "treemodel_dnd.h"
#include <iostream>

TreeModel_dnd::TreeModel_dnd()
{
	set_column_types(columns);
}

Glib::RefPtr<TreeModel_dnd> TreeModel_dnd::create()
{
	return Glib::RefPtr<TreeModel_dnd>(new TreeModel_dnd());
}

bool TreeModel_dnd::row_draggable_vfunc(const Gtk::TreeModel::Path &path) const
{
	auto unconst_this = const_cast<TreeModel_dnd *>(this);
	const_iterator iter = unconst_this->get_iter(path);
	if (iter) {
		Row row = *iter;
		bool is_draggable = row[columns.col_draggable];
		return is_draggable;
	}

	return Gtk::TreeStore::row_draggable_vfunc(path);
}

bool TreeModel_dnd::row_drop_possible_vfunc(
		const Gtk::TreeModel::Path &dest,
		const Gtk::SelectionData &selectiondata) const
{
	Gtk::TreeModel::Path dest_parent = dest;
	bool dest_is_not_top_level = dest_parent.up();
	if (!dest_is_not_top_level || dest_parent.empty()) {
		/* the user wants to move something to the top-level */
	} else {
		auto unconst_this = const_cast<TreeModel_dnd *>(this);
		const_iterator iter_dest_parent = unconst_this->get_iter(dest_parent);
		if (iter_dest_parent) {
			Row row = *iter_dest_parent;
			bool receives_drags = row[columns.col_receives_drags];
			return receives_drags;
		}
	}

	return Gtk::TreeStore::row_drop_possible_vfunc(dest, selectiondata);
}
