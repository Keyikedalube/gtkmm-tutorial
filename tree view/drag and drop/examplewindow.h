#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include "treemodel_dnd.h"
#include <gtkmm.h>

class ExampleWindow: public Gtk::Window {
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		/* signal handlers */
		void on_button_quit();

		/* child widgets */
		Gtk::Box v_box;

		Gtk::ScrolledWindow scrolledwindow;
		Gtk::TreeView treeview;
		Glib::RefPtr<TreeModel_dnd> ref_treemodel;

		Gtk::ButtonBox buttonbox;
		Gtk::Button button_quit;
};

#endif // GTKMM_EXAMPLEWINDOW_H
