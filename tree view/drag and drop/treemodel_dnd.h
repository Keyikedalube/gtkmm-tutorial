#ifndef GTKMM_TREEMODEL_DND_H
#define GTKMM_TREEMODEL_DND_H

#include <gtkmm.h>

class TreeModel_dnd: public Gtk::TreeStore {
	protected:
		TreeModel_dnd();

	public:
		/* tree model columns */
		class ModelColumns: public Gtk::TreeModel::ColumnRecord {
			public:
				ModelColumns()
				{
					add(col_id);
					add(col_name);
					add(col_draggable);
					add(col_receives_drags);
				}

				Gtk::TreeModelColumn<int> col_id;
				Gtk::TreeModelColumn<Glib::ustring> col_name;
				Gtk::TreeModelColumn<bool> col_draggable;
				Gtk::TreeModelColumn<bool> col_receives_drags;
		};

		ModelColumns columns;

		static Glib::RefPtr<TreeModel_dnd> create();

	protected:
		/* overridden virtual functions */
		bool row_draggable_vfunc(const Gtk::TreeModel::Path &) const override;
		bool row_drop_possible_vfunc(const Gtk::TreeModel::Path &, const Gtk::SelectionData &) const override;
};

#endif // GTKMM_TREEMODEL_DND_H
