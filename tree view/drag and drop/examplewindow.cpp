#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	v_box(Gtk::ORIENTATION_VERTICAL),
	button_quit("_Quit", true)
{
	set_title("Gtk::TreeView (Drag and drop) example");
	set_border_width(5);
	set_default_size(400, 200);

	add(v_box);

	scrolledwindow.add(treeview);
	scrolledwindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	v_box.pack_start(scrolledwindow);
	v_box.pack_start(buttonbox, Gtk::PACK_SHRINK);

	buttonbox.pack_start(button_quit, Gtk::PACK_SHRINK);
	buttonbox.set_border_width(5);
	buttonbox.set_layout(Gtk::BUTTONBOX_END);
	button_quit.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_quit));

	ref_treemodel = TreeModel_dnd::create();
	treeview.set_model(ref_treemodel);

	treeview.enable_model_drag_source();
	treeview.enable_model_drag_dest();

	Gtk::TreeModel::Row row = *(ref_treemodel->append());
	row[ref_treemodel->columns.col_id] = 1;
	row[ref_treemodel->columns.col_name] = "Billy Bob";
	row[ref_treemodel->columns.col_draggable] = true;
	row[ref_treemodel->columns.col_receives_drags] = true;
	
	Gtk::TreeModel::Row child_row = *(ref_treemodel->append(row.children()));
	child_row[ref_treemodel->columns.col_id] = 11;
	child_row[ref_treemodel->columns.col_name] = "Billy Bob Junior";
	child_row[ref_treemodel->columns.col_draggable] = true;
	child_row[ref_treemodel->columns.col_receives_drags] = true;

	child_row = *(ref_treemodel->append(row.children()));
	child_row[ref_treemodel->columns.col_id] = 12;
	child_row[ref_treemodel->columns.col_name] = "Sue Bob";
	child_row[ref_treemodel->columns.col_draggable] = true;
	child_row[ref_treemodel->columns.col_receives_drags] = true;
	
	row = *(ref_treemodel->append());
	row[ref_treemodel->columns.col_id] = 2;
	row[ref_treemodel->columns.col_name] = "Joey Jojo";
	row[ref_treemodel->columns.col_draggable] = true;
	row[ref_treemodel->columns.col_receives_drags] = true;
	
	row = *(ref_treemodel->append());
	row[ref_treemodel->columns.col_id] = 3;
	row[ref_treemodel->columns.col_name] = "Rob McRoberts";
	row[ref_treemodel->columns.col_draggable] = true;
	row[ref_treemodel->columns.col_receives_drags] = true;
	
	child_row = *(ref_treemodel->append(row.children()));
	child_row[ref_treemodel->columns.col_id] = 31;
	child_row[ref_treemodel->columns.col_name] = "Xavier McRoberts";
	child_row[ref_treemodel->columns.col_draggable] = true;
	child_row[ref_treemodel->columns.col_receives_drags] = true;
	
	treeview.append_column("ID", ref_treemodel->columns.col_id);
	treeview.append_column("Name", ref_treemodel->columns.col_name);
	treeview.append_column_editable("Draggable", ref_treemodel->columns.col_draggable);
	treeview.append_column_editable("Receives drags", ref_treemodel->columns.col_receives_drags);

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
	hide();
}
