#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include <gtkmm.h>

class ExampleWindow: public Gtk::Window
{
	public:
		ExampleWindow();
		virtual ~ExampleWindow();

	protected:
		// signal handlers
		void on_checkbutton_text();
		void on_checkbutton_activity();
		void on_checkbutton_inverted();

		bool on_timeout();
		void on_button_close();

		// child widgets
		Gtk::Box m_vbox;
		Gtk::Grid m_grid;
		Gtk::ProgressBar m_progressbar;
		Gtk::Separator m_separator;
		Gtk::CheckButton m_checkbutton_text, m_checkbutton_activity, m_checkbutton_inverted;
		Gtk::Button m_button_close;

		sigc::connection m_connection_timeout;
		bool m_bactivitymode;
};

#endif 	// GTKMM_EXAMPLEWINDOW_H
