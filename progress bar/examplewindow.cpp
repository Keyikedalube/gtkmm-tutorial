#include "examplewindow.h"
#include <iostream>

ExampleWindow::ExampleWindow():
	m_vbox(Gtk::ORIENTATION_VERTICAL, 5),
	m_checkbutton_text("Show text"),
	m_checkbutton_activity("Activity mode"),
	m_checkbutton_inverted("Right to left"),
	m_button_close("Close"),
	m_bactivitymode(false)
{
	set_resizable();
	set_title("Gtk::ProgressBar");

	m_vbox.set_border_width(10);
	add(m_vbox);

	m_vbox.pack_start(m_progressbar, Gtk::PACK_SHRINK, 5);
	m_progressbar.set_halign(Gtk::ALIGN_CENTER);
	m_progressbar.set_valign(Gtk::ALIGN_CENTER);
	m_progressbar.set_text("some text");
	m_progressbar.set_show_text(false);

	// add a timer callback to update the value of the progress bar
	m_connection_timeout = Glib::signal_timeout().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_timeout), 50);

	m_vbox.pack_start(m_separator, Gtk::PACK_SHRINK);
	m_vbox.pack_start(m_grid);
	m_grid.set_row_homogeneous(true);

	// add a check button to select displaying of the trough text
	m_grid.attach(m_checkbutton_text, 0, 0, 1, 1);
	m_checkbutton_text.property_margin() = 5;
	m_checkbutton_text.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbutton_text));

	// add a check button to toggle activity mode
	m_grid.attach(m_checkbutton_activity, 0, 1, 1, 1);
	m_checkbutton_activity.property_margin() = 5;
	m_checkbutton_activity.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbutton_activity));

	// add a check button to select growth from left to right or vice versa
	m_grid.attach(m_checkbutton_inverted, 0, 2, 1, 1);
	m_checkbutton_inverted.property_margin() = 5;
	m_checkbutton_inverted.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_checkbutton_inverted));

	// add a button to exit the program
	m_vbox.pack_start(m_button_close, Gtk::PACK_SHRINK);
	m_button_close.signal_clicked().connect(sigc::mem_fun(*this,
				&ExampleWindow::on_button_close));
	m_button_close.set_can_default();
	m_button_close.grab_default();

	show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_checkbutton_text()
{
	const bool show_text = m_checkbutton_text.get_active();
	m_progressbar.set_show_text(show_text);
}

void ExampleWindow::on_checkbutton_activity()
{
	m_bactivitymode = m_checkbutton_activity.get_active();

	if (m_bactivitymode)
		m_progressbar.pulse();
	else
		m_progressbar.set_fraction(0.0);
}

void ExampleWindow::on_checkbutton_inverted()
{
	const bool inverted = m_checkbutton_inverted.get_active();
	m_progressbar.set_inverted(inverted);
}

void ExampleWindow::on_button_close()
{
	hide();
}

/*
 * update the value of the progress bar so that we get some movement
 */

bool ExampleWindow::on_timeout()
{
	if (m_bactivitymode)
		m_progressbar.pulse();
	else {
		double new_val = m_progressbar.get_fraction() + 0.01;

		if (new_val > 1.0)
			new_val = 0.0;

		// set the new value
		m_progressbar.set_fraction(new_val);
	}

	// as this is a timeout function, return true so that it
	// continues to get called
	return true;
}
